//SE USA UNA FUNCION ANONIMA.

window.onload = function() {

	var imagen = document.querySelector("#boton-animar");

	//EN LUGAR DE DECLARAR LA FUNCION EN UNA VARIABLE,
	//USAMOS UNA FUNCION ANOMINA
	//NOTE 	QUE window.onload usa tambien una funcion anonima.
	imagen.addEventListener("click", function() {

		var imagen = document.querySelector("img");
		imagen.src = "imagenes/happy_homer.jpg"
	});

}
